""" dataDownload.py
Author: Sven Kardol
Date: 20-Jun-2017"""

# imports
import datetime as dt
import readOrbitData as rod
import connections as con

import os
import ephem
import math
import requests

from pathlib import Path
from pytz import utc


# ---------------------------------------------------------------------------------------------------------------------#
# Check for the number of missing days from the files. And instruct to download the missing files ---------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def checkMissingDays(datapath, check_days):
    # today's date
    current_date = dt.date.today()

    # list of missing days
    missing_days_for_return = []
    for i in range(1, check_days + 1):
        # determine the day before today's date
        test_day = current_date - dt.timedelta(days=i)

        # path to data file for specific date
        p = Path("{}Data/{}/{}/DailyTLE_{}".format(datapath, test_day.year, test_day.month, test_day))

        # check if there is a file, if not, add to missing days
        if not p.is_file():
            missing_days_for_return.append(test_day)

    # return all missing data days
    return missing_days_for_return


# ---------------------------------------------------------------------------------------------------------------------#
# Login to Space-track.org using credentials --------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def loginSpaceTrack():
    # get user and password from different file, which isn't stored online for protection purposes
    user, pswd = con.space_track()

    # have the login information combined
    payload_login = {
        'identity': user,
        'password': pswd
    }

    # open a session, so that there is no need to login multiple times to download more than 1 file.
    with requests.Session() as session:
        url_login = "https://www.space-track.org/ajaxauth/login"

        try:
            session.get(url_login, timeout=5)

            # use login credentials to login.
            session.post(url_login, data=payload_login)
        except (TimeoutError, ConnectionError, requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) \
                as e:
            status_report('loginSpaceTrack', e)
            return 0

    return session


# ---------------------------------------------------------------------------------------------------------------------#
# Check if folders exists, if not create it ---------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def folderExists(datapath, folder_year, folder_month):
    data_folder = Path(datapath + "Data")

    try:
        # check if the 'Data' folder exists, if not create it
        if not data_folder.is_dir():
            os.mkdir(datapath + "Data")

        # check if the 'Year' folder exists, if not create it
        year_folder = Path("{}Data/{}".format(datapath, folder_year))
        if not year_folder.is_dir():
            os.mkdir("{}Data/{}".format(datapath, folder_year))

        # check if the 'Month' folder exists, if not create it
        month_folder = Path("{}Data/{}/{}".format(datapath, folder_year, folder_month))
        if not month_folder.is_dir():
            os.mkdir("{}Data/{}/{}".format(datapath, folder_year, folder_month))

        # passed all checks, or has created the correct folders: return
        return
    except OSError:
        status_report('folderExists', 'OSError thrown. Check manually')

    return


# ---------------------------------------------------------------------------------------------------------------------#
# status_report messages, saved to local file for inspection ----------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def status_report(function_name, message):
    datapath = datapath = '/your/data/path/'
    # current time and date
    time = dt.datetime.now()
    date = dt.date.today()

    # check if the status folder exists
    data_folder = Path(datapath + 'Status')
    if not data_folder.is_dir():
        os.mkdir(datapath + 'Status')

    # check if a status file exists for today
    p = Path('{}Status/Status_{}'.format(datapath, date))
    if not p.is_file():
        f = open('{}Status/Status_{}'.format(datapath, date), "w+")
        f.write('[{}]: {} with the message: {}\n'.format(time, function_name, message))
        f.close()
    else:
        f = open('{}Status/Status_{}'.format(datapath, date), "a+")
        f.write('[{}]: {} with the message: {}\n'.format(time, function_name, message))
        f.close()

    print('[{}]: {} with the message: {}'.format(time, function_name, message))
    return


# ---------------------------------------------------------------------------------------------------------------------#
# Find the number of the latest catalog entry--------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def lastCatalogEntry():
    sat_dict = rod.readSatCat()
    latest = max(sat_dict)
    return latest


# ---------------------------------------------------------------------------------------------------------------------#
# Solar position ------------------------------------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def solarPosition(**key):

    # determine if there is a specific date entered in the function
    if 'time' in key:
        date = dt.datetime.strptime(key['time'], '%Y-%m-%d')
        time = date.replace(hour=12, minute=00).replace(tzinfo=utc)
    else:
        time = dt.datetime.utcnow()

    # greenwich position
    greenwich = ephem.Observer()
    greenwich.lat = "0"
    greenwich.lon = "0"
    greenwich.date = time
    sun = ephem.Sun(greenwich)
    sun.compute(greenwich.date)
    sun_lon = math.degrees(sun.ra - greenwich.sidereal_time())
    # limit the longitude between -180 and 180 degrees.
    if sun_lon < -180.0:
        sun_lon = 360.0 + sun_lon
    elif sun_lon > 180.0:
        sun_lon = sun_lon - 360.0
    sun_lat = math.degrees(sun.dec)
    time = time.replace(tzinfo=utc)
    print(time, sun_lon, sun_lat)
    return [time, sun_lon, sun_lat]


if __name__ == '__main__':
    solarPosition()
    test = lastCatalogEntry()
    print(test)
    status_report('test', 'This is a test message')

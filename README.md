Sven Kardol Thesis.

This is the README for my MSc. in Aerospace Engineering project. The project 
will be an Orbit visualization tool. 

The back-end software is written in python script. This script download 
the data needed for the tool.

In order to get the back-end working one would need another file which contains
the username and password for space-track.org. This file should look like this:

#connections.py

def space_track():
    space_track_username = "your username"
    space_track_password = "your password"
    return space_track_username,space_track_password
    

Another thing that you would have to do in order to get the tool running is to
change the directory in which the data is stored. There are 2 places to change
the directory:


-- in server.py on line 15

-- in support.py on line 106. 

(version of mid september 2018)
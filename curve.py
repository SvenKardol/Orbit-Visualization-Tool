import math

a = 7
b = 15.5
r = -4

time = []

step = 1000000
for i in range(0, step):
    time.append(math.pi / 2 + math.pi / step * i)

x = []
y = []
for i in time:
    x.append(a * math.cos(i) + b * math.cos(i) * r / (math.sqrt(b ** 2 * math.cos(i) ** 2 + a ** 2 * math.sin(i) ** 2)))
    y.append(b * math.sin(i) + a * math.sin(i) * r / (math.sqrt(b ** 2 * math.cos(i) ** 2 + a ** 2 * math.sin(i) ** 2)))

curve = 0
for i,t in enumerate(x):
    if i != 0:
        curve += math.sqrt((x[i]-x[i-1])**2+(y[i]-y[i-1])**2)

print(curve)
print('test')

from flask import Flask, render_template
from flask_jsglue import JSGlue

import readOrbitData as rod
import dataDownload as dd
import support as sup
import datetime as dt
# import ephem as ephem
import math

app = Flask(__name__)
jsglue = JSGlue(app)

# path is defined here and in the support.py status_report function. Have to change the second one as well.
datapath = '/your/data/path/'

@app.route('/')
def firstLoad():
    return render_template('first_load.html')

@app.route('/home')
def index():
    yesterday = dt.datetime.utcnow() - dt.timedelta(days=1)

    readOrbitalData = rod.readTLEData(datapath, yesterday.year, yesterday.month, yesterday.day)
    if readOrbitalData == None:
        # daily EOP download
        dd.dailyEOPDownload(datapath)

        # check for missing days in orbit data
        all_missing_days = sup.checkMissingDays(datapath, 15)

        # log in to space-track
        login_connection = sup.loginSpaceTrack()

        # daily SatCat download
        dd.dailySatCatDownload(datapath, login_connection)

        # test if the log in was successful
        if not login_connection == 0:
            for all_days in all_missing_days:
                dd.dailyDataDownload(datapath, login_connection, all_days)
            # end connection after the session is over.
            login_connection.close()

        # load todays data
        readOrbitalData = rod.readTLEData(datapath, yesterday.year, yesterday.month, yesterday.day)

    [time, sun_lon, sun_lat] = sup.solarPosition()
    satCat = rod.readSatCat(datapath)
    return render_template('template.html', data=readOrbitalData, time=time, sol_lon=sun_lon, sol_lat=sun_lat,
                           sat_cat=satCat)

@app.route('/date/<date>')
def userDate(date):
    split_date = date.split('-')
    try:
        year = int(split_date[0])
        month = int(split_date[1])
        day = int(split_date[2])
        readOrbitalData = rod.readTLEData(datapath, year, month, day)
    except (TypeError, ValueError) as e:
        print('error: {}'.format(e))
        return render_template('error.html')

    [time, sun_lon, sun_lat] = sup.solarPosition(time=date)
    satCat = rod.readSatCat(datapath)

    return render_template('template.html', data=readOrbitalData, time=time, sol_lon=sun_lon, sol_lat=sun_lat,
                           sat_cat=satCat)


# Search for specific satellite.
# @app.route('/specific/<satellite>+<start_date>+<end_date>')
# def userSpecific(satellite, start_date, end_date):
#     try:
#         login_connection = rod.loginSpaceTrack()
#         if login_connection == 0:
#             render_template('error.html')
#         readOrbitalData = dd.specificDataDownload(login_connection, satellite, start_date, end_date)
#     except (TypeError, ValueError) as e:
#         print('error: {}'.format(e))
#         return render_template('error.html')
#
#     [time, sun_lon, sun_lat] = sup.solarPosition(time=start_date)
#     satCat = rod.readSatCat(datapath)
#
#     return render_template('template.html', data=readOrbitalData, time=time, sol_lon=sun_lon, sol_lat=sun_lat,
#                            sat_cat=satCat)


@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/error')
def error():
    return render_template('error.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

""" dataDownload.py
Author: Sven Kardol
Date: 1-Jul-2017"""

# imports
from dataDownload import *
import support


# ---------------------------------------------------------------------------------------------------------------------#
# Read satellite catalog ----------------------------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def readSatCat(datapath):
    # check to see if the satcat is up to date.
    connection = support.loginSpaceTrack()
    dailySatCatDownload(datapath, connection)

    try:
        file_location = datapath + 'SatelliteCatalog/SatCat.txt'
        satcat_file = Path(file_location)

        # check if file exists
        if satcat_file.is_file():
            file = open(file_location, 'r')
            return file.read()
        else:
            # file doesn't exist, thus need to download the catalog.
            dailySatCatDownload(datapath,connection)
            support.status_report('readSatCat()', 'The file did not exist')

            file_location = datapath + 'SatelliteCatalog/SatCat_old.txt'
            satcat_file = Path(file_location)

            if satcat_file.is_file():
                file = open(file_location, 'r')
                return file.read()
    except (TypeError, IndexError) as e:
        support.status_report('readSatCat()', 'Due to a {} an exception was thrown'.format(e))
    return


# ---------------------------------------------------------------------------------------------------------------------#
# Read Earth Orientatation Paramaters----------------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def readEOP(datapath):

    data_folder = Path(datapath + "EarthOrientationParameters")
    # find out whether or not it is a folder
    if not data_folder.is_dir():
        # not a folder, means the data is not available. Therefore start downloading satcat again.
        dailyEOPDownload(datapath)

    try:
        file_location = datapath + 'EarthOrientationParameters/EOP.txt'
        EOP_file = Path(file_location)

        # check if file exists
        if EOP_file.is_file():

            # open EOP file and read all lines.
            with open(file_location) as f:
                EOP = f.readlines()

            EOP_dict = {}
            start = False
            for param in EOP:
                if start:
                    # end with the last observed parameters.
                    if param == 'END OBSERVED\n':
                        return EOP_dict
                    # obtain variables that lie within the catalog entry
                    year = param[:4].strip()
                    month = param[5:7].strip()
                    day = param[8:10].strip()

                    MJD = param[11:16].strip()
                    x = param[17:26].strip()
                    y = param[27:36].strip()
                    UT1UTC = param[37:47].strip()
                    LOD = param[48:58].strip()
                    deltapsi = param[59:68].strip()
                    deltaeps = param[69:78].strip()
                    deltaX = param[79:88].strip()
                    deltaY = param[89:98].strip()
                    deltaAT = param[100:102].strip()

                    # describe date
                    date = '{}-{}-{}'.format(year, month, day)

                    # enter these into the dictionary
                    EOP_dict[date] = {'Year': year, 'Month': month, 'Day': day, 'Modified Julian Date': MJD, 'x': x,
                                      'y': y, 'UT1-UTC': UT1UTC, 'Length of Day': LOD, ' deltapsi': deltapsi,
                                      'deltaepsilon': deltaeps, 'deltaX': deltaX, 'deltaY': deltaY,
                                      'Delta Atomic Time': deltaAT}

                else:
                    if param == 'BEGIN OBSERVED\n':
                        start = True

        else:
            # file doesn't exist, thus need to download the catalog.
            dailyEOPDownload(datapath)
    except (TypeError, IndexError) as e:
        support.status_report('readEOP()', 'Due to a {} an exception was thrown'.format(e))


# ---------------------------------------------------------------------------------------------------------------------#
# Read Daily orbit data :old version with information displayed wrongly. ----------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def readData(datapath, year, month, day):
    data_folder = Path(datapath + "Data")
    # find out whether or not the satCat is a folder
    if not data_folder.is_dir():
        # not a folder, means the data is not available. Therefore start downloading data again.
        login = support.loginSpaceTrack()
        date = dt.datetime(year=year, month=month, day=day)
        dailyDataDownload(datapath, login, date)

    try:
        file_location = '{}Data/{}/{}/DailyTLE_{}-{}-{}'.format(datapath, year, int(month), year, str(month).zfill(2),
                                                              str(day).zfill(2))
        orbit_file = Path(file_location)

        # check if file exists
        if orbit_file.is_file():

            # open orbit file and read all lines.
            f = open(file_location)
            orbit_dict = {}
            for line1 in f:
                line2 = next(f)
                # check line 1
                number1 = line1[:1].strip()
                sat1 = line1[2:7].strip()

                # check line 2
                number2 = line2[:1].strip()
                sat2 = line2[2:7].strip()

                # check if the 2 lines are from the same satellite and in order
                if int(number1) == 1 and int(number2) == 2 and sat1 == sat2:
                    # process lines with the information.
                    classification = line1[7:8].strip()
                    launch_year = line1[9:11].strip()
                    launch_number = line1[11:14].strip()
                    launch_piece = line1[14:17].strip()
                    epoch_year = line1[18:20].strip()
                    epoch_day = line1[20:32].strip()
                    mean_motion_1 = line1[33:43].strip()
                    mean_motion_2 = line1[44:52].strip()
                    drag = line1[53:61].strip()
                    zero = line1[62:63].strip()
                    element = line1[64:68].strip()
                    check_sum = line1[68:69].strip()
                    inclination = line2[8:16].strip()
                    RAAN = line2[17:25].strip()
                    eccentricity = line2[26:33].strip()
                    omega = line2[34:42].strip()
                    mean_anomaly = line2[43:51].strip()
                    mean_motion = line2[52:63].strip()
                    revolution = line2[63:68].strip()
                    check_sum_2 = line2[68:69].strip()

                    # enter these pieces of information into the dictionary
                    orbit_dict[sat1] = {'NORAD Catalog Number': sat1, 'Classification': classification,
                                        'Launch Year': launch_year, 'Launch Number': launch_number, 'Launch Piece':
                                            launch_piece, 'Epoch Year': epoch_year, 'Epoch Day': epoch_day,
                                        'First Time Derivative of the Mean Motion divided by two': mean_motion_1,
                                        'Second Time Derivative of Mean Motion divided by six ': mean_motion_2,
                                        'BSTAR drag term': drag, 'The number 0': zero, 'Element set number': element,
                                        'Checksum line 1': check_sum, 'Inclination': inclination,
                                        'Right ascension of the ascending node': RAAN, 'Eccentricity': eccentricity,
                                        'Argument of perigee': omega, 'Mean Anomaly': mean_anomaly, 'Mean Motion':
                                            mean_motion, 'Revolution number at epoch': revolution, 'Checksum line 2':
                                            check_sum_2}

                else:
                    support.status_report('ReadData', 'Lines do not match. Something is wrong.')
            # return completed dictionary
            return orbit_dict

        else:
            # not a folder, means the data is not available. Therefore start downloading data again.
            login = support.loginSpaceTrack()
            date = dt.datetime(year=year, month=month, day=day)
            dailyDataDownload(datapath, login, date)
    except (TypeError, IndexError) as e:
        support.status_report('readData()', 'Due to a {} an exception was thrown'.format(e))


# ---------------------------------------------------------------------------------------------------------------------#
# Read TLE data -------------------------------------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def readTLEData(datapath, year, month, day):
    data_folder = Path(datapath + "Data")
    date = dt.datetime(year, month, day)
    use_date = date
    # find out whether or not the satCat is a folder
    if not data_folder.is_dir():
        # not a folder, means the data is not available. Therefore start downloading data again.
        login = support.loginSpaceTrack()
        dailyDataDownload(datapath, login, date)

    try:
        # start with an empty dictionary.
        orbit_dict = {}
        # find the data in the 5 days leading up to the chosen date.
        for i in range(0, 5):
            use_date -= dt.timedelta(days=1)
            file_location = '{}Data/{}/{}/DailyTLE_{}-{}-{}'.format(datapath, use_date.year, int(use_date.month), use_date.year,
                                                                  str(use_date.month).zfill(2),
                                                                  str(use_date.day).zfill(2))
            orbit_file = Path(file_location)

            # check if file exists
            if not orbit_file.is_file():
                # not a folder, means the data is not available. Therefore start downloading data again.
                login = support.loginSpaceTrack()
                date = dt.datetime(year=use_date.year, month=use_date.month, day=use_date.day)
                dailyDataDownload(datapath, login, date)

            # one last check
            orbit_file = Path(file_location)
            if orbit_file.is_file():
                # open orbit file and read all lines.
                f = open(file_location)

                # read the lines
                for line1 in f:
                    # check if there is a next line
                    try:
                        line2 = next(f)
                    except StopIteration:
                        continue

                    # check line 1
                    number1 = line1[:1].strip()
                    sat1 = line1[2:7].strip()

                    # check line 2
                    number2 = line2[:1].strip()
                    sat2 = line2[2:7].strip()

                    # check if the 2 lines are from the same satellite and in order
                    if int(number1) == 1 and int(number2) == 2 and sat1 == sat2:
                        # enter tle lines into the data.
                        try:
                            test_orbit = orbit_dict[sat1]
                        except KeyError:
                            orbit_dict[sat1] = {'tle1': line1.strip('\n'), 'tle2': line2.strip('\n')}
                    else:
                        support.status_report('ReadTLEData', 'Lines do not match. Something is wrong.')

        # return completed dictionary
        return orbit_dict

    except (TypeError, IndexError) as e:
        support.status_report('readTLEData()', 'Due to a {} an exception was thrown'.format(e))


if __name__ == '__main__':
    t = readSatCat()

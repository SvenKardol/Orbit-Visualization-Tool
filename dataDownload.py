""" dataDownload.py
Author: Sven Kardol
Date: 12-Jun-2017"""

# imports
import requests
import os
import datetime as dt
import support

from pathlib import Path


# ---------------------------------------------------------------------------------------------------------------------#
# Download file from missing day --------------------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def dailyDataDownload(datapath, log_in_connection, missing_date):
    next_day = missing_date + dt.timedelta(days=1)
    missing_date = missing_date.date()
    next_day = next_day.date()
    print(missing_date, next_day)
    support.folderExists(datapath, missing_date.year, missing_date.month)
    url = "https://www.space-track.org/basicspacedata/query/class/tle/EPOCH/{}%2000:00:00" \
          "--{}%2000:00:00/orderby/TLE_LINE1/format/tle".format(missing_date, next_day)

    # open the url using the open connection session
    try:
        response = log_in_connection.get(url, timeout=5)
    except (TimeoutError, ConnectionError, requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout,
            AttributeError) as e:
        support.status_report('dailyDataDownload', 'Connection error caused by {}.'.format(e))
        return

    # check to see if the response was good.
    if response.status_code == 200:
        try:
            text_file = open("{}Data/{}/{}/DailyTLE_{}".format(datapath, missing_date.year, missing_date.month, missing_date), "wb")
            text_file.write(response.content)
            text_file.close()
            support.status_report('dailyDataDownload', 'File {} download was successful.'.format(missing_date))
        except TypeError:
            support.status_report('dailyDataDownload', 'File {} download was unsuccessful. Due to TypeError.'.format(
                missing_date.date()))
    else:
        support.status_report('dailyDataDownload', 'File {} download was unsuccessful. Due to response error.'.format(
            missing_date))
    return


# ---------------------------------------------------------------------------------------------------------------------#
# Download file for missing day or specific satellite -----------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def specificDataDownload(log_in_connection, satellite, start_date, end_date):
    if satellite == 'all':
        satellite = '1--' + support.lastCatalogEntry()

    url = 'https://www.space-track.org/basicspacedata/query/class/tle/EPOCH/{}--{}/NORAD_CAT_ID/{}' \
          '/orderby/TLE_LINE1%20ASC/format/tle'.format(start_date, end_date, satellite)

    # open the url using the open connection session
    try:
        response = log_in_connection.get(url, timeout=5)
    except (TimeoutError, ConnectionError, requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout,
            AttributeError) as e:
        support.status_report('specificDataDownload', 'Connection error caused by {}.'.format(e))
        return

    sat_dict = {}
    if response.status_code == 200:
        satellites = iter(response.content.decode('ascii').split('\n'))
        for line1 in satellites:
            if line1 == '':
                return sat_dict
            else:

                # obtain variables that lie within the catalog entry
                line2 = next(satellites)

                # check line 1
                number1 = line1[:1].strip()
                sat1 = line1[2:7].strip()

                # check line 2
                number2 = line2[:1].strip()
                sat2 = line2[2:7].strip()

                # check if the 2 lines are from the same satellite and in order
                if int(number1) == 1 and int(number2) == 2 and sat1 == sat2:
                    sat_dict[sat1] = {'tle1': line1.strip('\n'), 'tle2': line2.strip('\n')}

        # return completed dictionary
        return sat_dict
    return sat_dict


# ---------------------------------------------------------------------------------------------------------------------#
# Download file for Earth Orientation Parameters-----------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def dailyEOPDownload(datapath):
    data_folder = Path(datapath + "EarthOrientationParameters")

    current_date = dt.date.today()

    # find out whether or not the EOP is a folder
    if not data_folder.is_dir():
        # make directory
        try:
            os.mkdir(datapath + 'EarthOrientationParameters')
        except OSError:
            support.status_report('dailyEOPDownload()', 'Directory cannot be created. Check manually.')
            return

    # check the age of the current file. if it is less than a day old, no need to download  a new file.
    eop_file = Path(datapath + 'EarthOrientationParameters/EOP.txt')
    if eop_file.is_file():
        try:
            # obtain the file modification time
            eop_stats = os.stat(datapath + 'EarthOrientationParameters/EOP.txt')
            file_age = eop_stats.st_mtime

            # check if the file is a day or older (86400 seconds)
            time_delta = dt.datetime.now().timestamp() - file_age
            if time_delta < 86400:
                return
        except OSError:
            support.status_report('dailyEOPDownload()', 'There is no EOP file to check for age')

    # download the satellite catalog
    try:
        response = requests.get('http://www.celestrak.com/SpaceData/eop20120101.txt', timeout=5)
    except (TimeoutError, ConnectionError, requests.exceptions.RequestException, requests.exceptions.ReadTimeout) as e:
        support.status_report('dailyEOPDownload()', 'Connection timed out or did not give a response. {}'.format(e))
        return

    if response.status_code == 200:
        # try to rename the old file to old, if this fails write a new file without renaming the old,
        # since it doesn't exist.
        try:
            os.rename(datapath + 'EarthOrientationParameters/EOP.txt', datapath + 'EarthOrientationParameters/EOP_old.txt')
        except OSError:
            support.status_report('dailyEOPDownload', 'Renaming the EOP download was unsuccessful. Due to OSError.')

        # write new file to the database
        try:
            text_file = open(datapath + "EarthOrientationParameters/EOP.txt", "wb")
            text_file.write(response.content)
            text_file.close()
            support.status_report('dailyEOPDownload', 'File {} download was successful.'.format(current_date))
        except TypeError:
            support.status_report('dailyEOPDownload', 'File {} download was unsuccessful. Due to TypeError.'.format(
                current_date))
    else:
        support.status_report('dailyEOPDownload', 'File {} download was unsuccessful. Due to a response '
                                                  'error.'.format(current_date))


# ---------------------------------------------------------------------------------------------------------------------#
# Download file for International Designators ==-----------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
def dailySatCatDownload(datapath, log_in_connection):
    data_folder = Path(datapath + "SatelliteCatalog")

    # find out whether or not the EOP is a folder
    if not data_folder.is_dir():
        # make directory
        try:
            os.mkdir(datapath + 'SatelliteCatalog')
        except OSError:
            support.status_report('dailySatCatDownload()', 'Directory cannot be created. Check manually.')
            return

    # check the age of the current file. if it is less than a day old, no need to download  a new file.
    satcat_file = Path(datapath + 'SatelliteCatalog/SatCat.txt')
    if satcat_file.is_file():
        # obtain the file modification time
        satcat_stats = os.stat(datapath + 'SatelliteCatalog/SatCat.txt')
        file_age = satcat_stats.st_mtime

        # check if the file is a day or older (86400 seconds)
        time_delta = dt.datetime.now().timestamp() - file_age
        if time_delta < 86400:
            return
        else:
            #     try to rename the old file to old, if this fails write a new file without renaming the old,
            # since it doesn't exist.
            try:
                os.rename(datapath + 'SatelliteCatalog/SatCat.txt', datapath + 'SatelliteCatalog/SatCat_old.txt')
            except OSError as e:
                support.status_report('dailySatCatDownload', 'Renaming the satellite catalog was unsuccessful. Due to '
                                                             '{}.'.format(e))

    # download the satellite catalog
    url = 'https://www.space-track.org/basicspacedata/query/class/satcat/orderby/NORAD_CAT_ID%20asc/metadata/false'

    # open the url using the open connection session
    try:
        log_in_connection = support.loginSpaceTrack()
        response = log_in_connection.get(url, timeout=5)
    except (TimeoutError, ConnectionError, requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout,
            AttributeError) as e:
        support.status_report('dailySatCatDownload', 'Connection error caused by {}.'.format(e))
        return

    if response.status_code == 200:
        try:
            text_file = open(datapath + 'SatelliteCatalog/SatCat.txt', "wb")
            text_file.write(response.content)
            text_file.close()
            support.status_report('dailySatCatDownload', 'SatCat download was successful.')
        except TypeError:
            support.status_report('dailySatCatDownload', 'SatCat download was unsuccessful. Due to TypeError.')
    else:
        support.status_report('dailySatCatDownload', 'SatCat download was unsuccessful. Due to response error.')


if __name__ == '__main__':
    # daily SatCat download
    # dailySatCatDownload()

    # daily EOP download
    # dailyEOPDownload()

    # check for missing days in orbit data
    # missing_days = checkMissingDays(15)

    # log in to space-track
    connection = support.loginSpaceTrack()
    dailySatCatDownload('/home/sven/Documents/Orbit-Visualization-Tool/', connection)

    # # specificDataDownload
    # specificDataDownload(connection, 'all', '1980-01-01', '1980-01-02')
    # # test if the log in was successful
    # if not connection == 0:
    #     for days in missing_days:
    #         dailyDataDownload(connection, days)
    #     # end connection after the session is over.
    #     connection.close()

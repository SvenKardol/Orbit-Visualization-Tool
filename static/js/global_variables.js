var time_factor = 1;
var time_factors = [-500, -100, -50, -20, -10, -5, -2, -1, 0, 1, 2, 5, 10, 20, 50, 100, 500, 3000];
var scene;
var particle_size = 2;

// buffer_orbit = 1 means that the buffer_time shows the number of orbits. Anything else adn buffer time is minutes.
var buffer_time = 1;
var buffer_orbit = 1;

var time_offset = 0;
var reference_frame = 'ECI';
var solar_delta = 0;
var GMT_time, satcat, camera, search_satellites, display_orbit, raycaster, current_satellite, width, height;
var search_positions;
var refreshed = false;
var update_used_ids = true;
var search_list = [];

// determine which satellites are visible to the user and what colors
var specific_ids = [];
var groups = ['DEBRIS', 'TBA', 'ROCKET BODY', 'PAYLOAD'];
var group_colors = {'DEBRIS': new THREE.Color(0xFF0000), 'TBA': new THREE.Color(0x0000FF),
                    'ROCKET BODY': new THREE.Color(0xFF00FF), 'PAYLOAD': new THREE.Color(0x00AA00) }


// toggle information on and off for better screenshots
var information_visible = true;

// color of orbit for searches in order of super_count, can add for more variation
var super_count = -1;
var super_colors = [new THREE.Color(0xFF0000), new THREE.Color(0x00FF00), new THREE.Color(0x00FFFF)]

function arrayColumn(arr, n) {
  return arr.map(x=> x[n]);
}

(function () {
    // check WebGL compatibility
    var webglEl = document.getElementById('webgl');

	if (!Detector.webgl) {
	    document.getElementById('message').innerHTML = 'WebGL failed. The visualization cannot be run with your current setup.'
		Detector.addGetWebGLMessage(webglEl);
		return;
	}

    // Some screen and zoom variables
	var width  = window.innerWidth,
		height = window.innerHeight,
		angle  = 45,
		near   = 4.000,
		far    = 450.000,
		raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

    //  date
    GMT_time = new Date(document.getElementById('time').innerHTML);
    start_time = new Date();
    console.log(start_time, GMT_time)
    solarTime = GMT_time;

    // make scene
	scene = new THREE.Scene();
//	scene.background = new THREE.Color(0xFFFFFF)

    // set camera
	camera = new THREE.PerspectiveCamera(angle, width/height, near/5, far*5);
	camera.up = new THREE.Vector3( 0, 0, 1 ); // rotate coordinates.
	camera.position.x = 50;
	camera.name = 'camera';

    // create renderer
	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(width, height);
    // add renderer as child
	webglEl.appendChild(renderer.domElement);

    // add ambient light to the scene
    ambient = new THREE.AmbientLight(0x333333);
    ambient.name = 'ambient light'
    scene.add(ambient);

    // create a light source
	var light = new THREE.DirectionalLight(0xFFFFFF, 1);

	// determine position of the light depending on the time and date: solar position
	solarLon = parseFloat(document.getElementById('sol_lon').innerHTML)/180*Math.PI;
	solarLat = parseFloat(document.getElementById('sol_lat').innerHTML)/180*Math.PI;

	[lightX, lightY, lightZ] = sunPosition(0, solarLon, solarLat);
	light.name = 'sun light';

	// set light to correct position and add to the scene
	scene.add(light);
	light.position.set(far*lightX, far*lightY, far* lightZ);
//    light.position.set(0,0,50)

    // create a sphere and position at center of coordinate system
    // 2 options: fast rendering a cinematic
    earth_simple = createSimpleEarth();
    earth_complex = createEarth();
    earth = earth_simple;
    scene.add(earth)

    // add controls for the scene
    var controls = new THREE.OrbitControls( camera, webglEl );
//	var controls = new THREE.TrackballControls(camera, webglEl);
    controls.enableDamping = true;
    controls.dampingFactor = 0.6;
    controls.enableZoom = true;
    controls.zoomSpeed = 4;
	controls.minDistance = near*2;
    controls.maxDistance = far-near;
    controls.enablePan = false;

    // create sub-solar position (ssp) cube that is located at the earths radius
    var sspGeometry = new THREE.Geometry();
    sspGeometry.vertices.push(new THREE.Vector3(0,0,0));
    var sspMaterial = new THREE.PointsMaterial({
              map: THREE.ImageUtils.loadTexture('../static/img/sun.png'),
              transparent: true,
              size: 1
            });
    var ssp = new THREE.Points(sspGeometry, sspMaterial);
    ssp.name = 'sub solar point';
    scene.add(ssp);
    ssp.position.set(0,0,0);
    sspGeometry.vertices[0].set(lightX*(radius+0.1), lightY*(radius+0.1), lightZ*(radius+0.1));

    // obtain all satellites and information from the data
	addSatellites();
	console.log(ids);

    display_orbit = new THREE.Object3D();
    display_orbit.name = 'display orbit'
    scene.add(display_orbit);
    search_satellites = new THREE.Object3D();
    search_satellites.name = 'search satellites'
    scene.add(search_satellites);
    search_positions = new THREE.Object3D();
    search_positions.name = 'search positions';
    scene.add(search_positions);


    // variables used in the render
    var count = -1;
    var frame_rate = 0, performance_previous = 0, performance_now = 0;

    // set variables for information boxes
    var all_satellites = true;
    var current_satellite = 0;
    var satellite_hover = document.getElementById('satellite_hover');
    var satellite_info = document.getElementById('satellite_info');
    var satellite_info_name = document.getElementById('satellite_name');
    var satellite_info_norad = document.getElementById('satellite_norad');
    var satellite_info_intdes = document.getElementById('satellite_intdes');
    var satellite_info_type = document.getElementById('satellite_type');
    var satellite_info_apogee = document.getElementById('satellite_apogee');
    var satellite_info_perigee = document.getElementById('satellite_perigee');
    var satellite_info_inclination = document.getElementById('satellite_inclination');
    var satellite_info_period = document.getElementById('satellite_period');
    var satellite_info_decay = document.getElementById('satellite_decay');

    // mouse movement eventlistener.
    window.addEventListener( 'mousemove', onMouseMove, false );
    window.addEventListener( 'mouseclick', onMouseClick, false);


    // run visualization
    render();

    function onMouseMove( event ) {
        mouse.x = ( event.clientX / width ) * 2 - 1;
        mouse.y = - ( event.clientY / height ) * 2 + 1;
        rayCasterFunction();
    };

    function onMouseClick ( event ){
        mouseclick.x = ( event.clientX / width ) * 2 - 1;
        mouseclick.y = - ( event.clientY / height ) * 2 + 1;
    };

	function render() {
        count+=1;

        // calculate the frame rate over 10 frames (less jumpy instead of every frame)
	    if (count%10==0){
            performance_now = performance.now();
            frame_rate = Math.floor(10000/(performance_now - performance_previous));
            performance_previous = performance_now;
        }

	    delta = time_factor*(new Date().getTime() - start_time.getTime())/1000 + time_offset;
	    time_offset = 0;
	    solar_delta += delta;
	    simulation = GMT_time.getTime()+solar_delta*1000;
        start_time = new Date();
        var write_date = new Date(simulation)
        document.getElementById('time').innerHTML = write_date.toUTCString().replace('GMT', 'UTC') + '  Frame rate: ' + frame_rate;

        // solar position update
        if (reference_frame == 'ECEF'){
            [lightX, lightY, lightZ] = sunPosition(solar_delta, solarLon, solarLat);
            light.position.set(far*lightX, far*lightY, far* lightZ);
            sspGeometry.vertices[0].set(lightX*(radius+0.1), lightY*(radius+0.1), lightZ*(radius+0.1));
            sspGeometry.verticesNeedUpdate = true;
            earth.rotation.z = 0;
        }else{
            var solar_position = sunPosition(solar_delta, solarLon, solarLat);
            solarposition = {'x': solar_position[0], 'y':solar_position[1], 'z':solar_position[2]};
            var gmst = satellite.gstimeFromDate(new Date(simulation));
            var solar_position_ECI = satellite.ecfToEci(solarposition, gmst);
            lightX = solar_position_ECI.x;
            lightY = solar_position_ECI.y;
            lightZ = solar_position_ECI.z;
            light.position.set(far*lightX, far*lightY, far* lightZ);
            sspGeometry.vertices[0].set(lightX*(radius+0.1), lightY*(radius+0.1), lightZ*(radius+0.1));
            sspGeometry.verticesNeedUpdate = true;

            var earth_orientation = {'x':6.378, 'y': 0, 'z': 0 };
            var earth_orientation_ECI = satellite.ecfToEci(earth_orientation, gmst)
            earth_angle = Math.atan2(earth_orientation_ECI.y, earth_orientation_ECI.x);
            earth.rotation.z = earth_angle;
        }

        // update satellite and orbits that the satellites travel
        if (update_used_ids){
            if (specific_ids.length == 0){
                all_satellites = true;
                used_ids = ids;
            }else{
                all_satellites = false;
                used_ids = specific_ids;
            }
            update_used_ids = false;
        }

        for (var id in ids){
            var object = ids[id];

            if (all_satellites || used_ids.includes(object)){
                var object_userData = object[1];
                var time_0 = object_userData.start_time;
                var time_delta = (solar_delta)/60;
                var Lx = object_userData.Lx;
                var Ly = object_userData.Ly;
                var Lz = object_userData.Lz;

                var update = 0, new_x = 0, new_y = 0, new_z = 0, status_x = 0, status_y = 0, status_z = 0;

                try {
                    [new_x,status_x] = Lx.position(time_delta);
                    [new_y,status_y] = Ly.position(time_delta);
                    [new_z,status_z] = Lz.position(time_delta);
                }catch(ex){
                    console.log(object, object_userData.Lx, Lx);
                    console.log(ex);
                }

                var new_position = new THREE.Vector3(new_x,new_y,new_z);

                satellites.geometry.vertices[id] = new_position;

                // determine if the current buffer is still sufficient
                if (status_x == 1 || status_y == 1 || status_z ==1){
                    update = 1;
                }

                // insufficient buffer: update the buffer
                if (update == 1){
                    try{
                        var [update_sat, update_orbit] = satelliteUpdate(object_userData, simulation);
                    }catch(ex){
                        console.log(object, object_userData.Lx, Lx);
                        console.log(ex);

                    }
                    if (update_sat == 0){
                        console.log('something goes wrong');
                        console.log(object, object_userData.Lx, Lx);
                    }else{
                        object_userData = update_sat;

                        if (update_orbit != 0){
                            satellite_orbits[id] = update_orbit;

                            for (var qq in display_orbit.children){
                                if(id == display_orbit.children[qq].sat_index){

                                    while (display_orbit.children.length > 0){
                                        display_orbit.remove(display_orbit.children[0])
                                    }
                                    var temp_orbit = update_orbit;
                                    temp_orbit.sat_index = id;
                                    display_orbit.add(temp_orbit);
                                    try{
                                        temp_orbit.material = new THREE.LineBasicMaterial({color: 0x0000FF});
                                    }catch(ex){
                                        console.log(ex, display_orbit, sat_index);
                                    }
                                }
                            }

                            for (var qq in search_satellites.children){
                                var id_check = 'search_'+ids[id][0];
                                if(id_check == search_satellites.children[qq].name){
                                    search_satellites.remove(search_satellites.children[qq])
                                    var temp_orbit = update_orbit;
                                    try{
                                        temp_orbit.name = "search_"+ids[id][0];
                                        search_satellites.add(temp_orbit);
                                        // TODO: make this color the same as the color searched
                                        temp_orbit.material = new THREE.LineBasicMaterial({color: 0xFFFF00});
                                    }catch(ex){}
                                }
                            }

                        }


                    }

                }

            }else{
                var new_position = new THREE.Vector3(0,0,0);
                satellites.geometry.vertices[id] = new_position;
            }
        }

        satellites.geometry.verticesNeedUpdate = true;

		controls.update();

		requestAnimationFrame(render);
		renderer.render(scene, camera);
	};

	function rayCasterFunction(){
    // update the picking ray with the camera and mouse position
    raycaster.setFromCamera(mouse, camera);

    // calculate objects intersecting the picking ray
    var intersects = raycaster.intersectObjects([satellites]);

    // if there are intersection find the best intersection and use the data from that satellite
    if (intersects.length > 0){
        var index = 0;
        for (var i in intersects){
            if (intersects[i].distanceToRay < intersects[index].distanceToRay){
                index = i;
            }
        }
        var sat_index = intersects[index].index;
        if (sat_index != current_satellite && (all_satellites || used_ids.includes(ids[sat_index]))){
            while (display_orbit.children.length > 0) {
                display_orbit.remove(display_orbit.children[0]);
            }
            temp_orbit = satellite_orbits[sat_index].clone();
            temp_orbit.sat_index = sat_index;
            if (information_visible){
                display_orbit.add(temp_orbit)
                try{
                    temp_orbit.material.color.setHex( 0x0000FF );
                }catch(ex){
                    console.log(ex, display_orbit, sat_index);
                }

                satellite_hover.style.left = (mouse.x+1)/2*width+15+"px";
                satellite_hover.style.top = -(mouse.y-1)/2*height+15+"px";
                var satcat_index = ids[sat_index][1].satnum-1;
                var satcat_info = satcat[satcat_index]
                satellite_hover.innerHTML = satcat_info['SATNAME'];
                satellite_info_name.innerHTML = satcat_info['SATNAME'];
                satellite_info_norad.innerHTML = satcat_info['NORAD_CAT_ID'];
                satellite_info_intdes.innerHTML = satcat_info['INTLDES'];
                satellite_info_type.innerHTML = satcat_info['OBJECT_TYPE'];
                satellite_info_apogee.innerHTML = satcat_info['APOGEE']+" km";
                satellite_info_perigee.innerHTML = satcat_info['PERIGEE']+" km";
                satellite_info_inclination.innerHTML = satcat_info['INCLINATION']+ '&#186;';
                satellite_info_period.innerHTML = satcat_info['PERIOD']+ " minutes/ "+ Math.round(satcat_info['PERIOD']/60 * 100) / 100+ ' hours';
                satellite_info_decay.innerHTML = satcat_info['DECAY'];

                current_satellite = sat_index;
            }
        }
    }
};

}());
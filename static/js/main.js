// page loading functions
$(document).ready(function() {
    document.getElementById('UserSearch').style.display = "none";
    $("#userDate").datepicker({ dateFormat: 'yy/mm/dd' });
    $("#userDateStart").datepicker({ dateFormat: 'yy/mm/dd' });
    $("#userDateEnd").datepicker({ dateFormat: 'yy/mm/dd' });

});


// Function to be executed on load. This checks if there needs to be a dailyDownload and displays the first information
function loadingData(show){
        if (show == 0){
//            document.getElementById("page").style.display = "none";
            document.getElementById("loading").style.display = "none";
            document.getElementById("satellite_info").style.display = "block";
            document.getElementById("satellite_hover").style.display = "block";
        }else if (show == 23){
//            document.getElementById("page").style.display = "none";
            document.getElementById("loading").style.display = "none";
            document.getElementById("satellite_info").style.display = "none";
            document.getElementById("satellite_hover").style.display = "none";
        }else {
//            document.getElementById("page").style.display = "none";
            document.getElementById("loading").style.display = "block";
            document.getElementById("satellite_info").style.display = "none";
            document.getElementById("satellite_hover").style.display = "none";
        }
};

// Function to obtain the correct date. If inputDate = 0 then its today
function searchDate(inputDate){
    var today= new Date();
    today.setDate(today.getDate() - 1);

    if (inputDate == 0){
        var date_today = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var url = "/date/" + date_today;
    }else if (inputDate == 2){
        var satellite_search = document.getElementById('userSatelliteDate').value;
        if (satellite_search == ""){
            satellite_search = 'all';
        }

        var satellite_search_returned = searchSatelliteCatalog(satellite_search)

        var date_start = document.getElementById('userDateStart').value;
        var date_end = document.getElementById('userDateEnd').value;
        date_start =  date_start.replace('/','-');
        date_start =  date_start.replace('/','-');
        date_end =  date_end.replace('/','-');
        date_end =  date_end.replace('/','-');

        var url = "/specific/" + satellite_search_returned + "+" + date_start + "+" + date_end;

    }else{
        var date_input = document.getElementById('userDate').value;
        console.log(date_input);
        date_input =  date_input.replace('/','-');
        date_input =  date_input.replace('/','-');
        console.log(date_input);
        var url = "/date/" + date_input;
    }

    document.getElementById("UserSearch").style.display="none";
    // Set the url to the desired date

    window.location.href = window.location.origin + url;
};

function searchSatelliteCatalog(search_term){
    console.log(search_term)
    if (search_term == "" || search_term == 'all'){
        return search_term;
    }


    console.log(search_term);
    var search_terms = search_term.split("+");

    var search_list = [];
    var search_count = 0;

    var extra_orbit_number = -1;
    for (var j in search_terms){
        for (var i in satcat){

            if (satcat[i]['NORAD_CAT_ID'] == search_terms[j]){
                var extra_orbit_number = i;

            }else if (satcat[i]['INTLDES'] == search_terms[j]){
                var extra_orbit_number = i;

            }else if (satcat[i]['SATNAME'].includes(search_terms[j].toUpperCase())){
                var extra_orbit_number = i;
            }

            if (extra_orbit_number != -1 && extra_orbit_number != -2){
                search_count += 1;
                search_list.push(satcat[i]['NORAD_CAT_ID'])
                extra_orbit_number = -2;

            }
        }

        if (extra_orbit_number == -1){
            console.log('Nothing matched.');
        }else{
            var return_string = search_list[0];
            for (var i in search_list){
                if (i != 0){
                    return_string += "," + search_list[i]
                }
            }
            if (search_count == 1){
                console.log('There was 1 match found');
            }else{
                    console.log('There were ' + search_count +" matches found.");
            }

            return return_string;
        }
    }
};

function searchSatelliteResults(search_term){
    console.log(search_term)

//    delete all search_results
    var search_results = document.getElementById('satellite-results')
    search_results.style.display = "block";
    while (search_results.firstChild) {
        search_results.removeChild(search_results.firstChild);
    }
//    minimum length of the search is 3
    if (search_term.length < 3){
        search_results.style.display = "none";
            return search_term
    }

    search_list = [];
    var search_count = 0;

    var extra_orbit_number = -1;

    console.log('start search')
    var test_ids = arrayColumn(ids, 0)
    console.log(test_ids)

    for (var i in satcat){
        if (satcat[i]['NORAD_CAT_ID'] == search_term){
            var extra_orbit_number = i;

        }else if (satcat[i]['INTLDES'] == search_term){
            var extra_orbit_number = i;

        }else if (satcat[i]['SATNAME'].includes(search_term.toUpperCase())){
            var extra_orbit_number = i;
        }

        if (extra_orbit_number != -1 && extra_orbit_number != -2){
            var test_number = parseInt(i)+1
            var test_string = 'orbit_'+ test_number;
            var test_position = $.inArray(test_string, test_ids)
            if (test_position != -1){
                try{
                    var object = ids[test_position];
                    var object_userData = object[1];
                    search_list.push([satcat[i]['SATNAME'],satcat[i]['NORAD_CAT_ID']]);
                }catch(ex){};
            }
            extra_orbit_number = -2;
        }
    }
    console.log('end search')
    console.log(search_list.length)

    if (search_list.length == 0){
        search_results.style.display = "none";
        return search_term
    }else{
        super_count += 1;
    }

    for (var i in search_list){
        var element = document.createElement("DIV");
        element.className = 'search-result';
        element.id = search_list[i][1];
        var p = document.createElement("P");
        var t = document.createTextNode(search_list[i][0]+" ("+search_list[i][1]+")");
        p.appendChild(t)

        var show_button = document.createElement("BUTTON");
        var image = document.createElement("IMG");
        image.src="../static/img/focus.png";
        image.className='button_image'
        show_button.appendChild(image);
        show_button.value = search_list[i][1];
        show_button.title = 'Bring in focus';
        show_button.id = 'show_' + search_list[i][1];
        show_button.color_current = 'white';
        show_button.onclick = function(){cameraView(this.value)};

        var orbit_button = document.createElement("BUTTON");
        var image2 = document.createElement("IMG");
        image2.className='button_image'
        image2.src="../static/img/sat_orbit.png";
        orbit_button.appendChild(image2);
        orbit_button.value = search_list[i][1];
        orbit_button.id = 'button_' + search_list[i][1];
        orbit_button.title = 'Show orbit';
        orbit_button.color_current = 'white';
        orbit_button.onclick = function(){orbitShow(this.value)};

        var position_button = document.createElement("BUTTON");
        var image3 = document.createElement("IMG");
        image3.className='button_image'
        image3.src="../static/img/satellite.png";
        position_button.appendChild(image3);
        position_button.value = search_list[i][1];
        position_button.id = 'position_' + search_list[i][1];
        position_button.title = 'Show satellite';
        position_button.color_current = 'white';
        position_button.onclick = function(){positionShow(this.value)};

        element.appendChild(p);
        element.appendChild(show_button);
        element.appendChild(orbit_button);
        element.appendChild(position_button)
        document.getElementById('satellite-results').appendChild(element);
    }

}

// Function to obtain the data from the back end
function getData(){
    try {
        console.log(window.test11)
        data1 = window.test11
        document.getElementById('text').innerHTML = data1;
    } catch (e) {
        document.getElementById('text').innerHTML = 'nope';
    }
};

// Function to determine if the date inputted is a correct date.
function checkInputDate(searchValue){
    document.getElementById('searchMessage').innerHTML = '';
    if (searchValue ==0){
        var inputDate = document.getElementById('userDate').value;
    }else if (searchValue ==1){
        var inputDate = document.getElementById('userDateStart').value;
        var inputDate2 = document.getElementById('userDateEnd').value;
    }

    var date_input = new Date(inputDate);
    var date_input2 = new Date(inputDate2);
    if (date_input.getTime() == date_input.getTime() && date_input.getFullYear() >= 1950){
        if (searchValue == 0){
            document.getElementById('confirmDate').disabled = false;
            console.log(date_input);
        }else if (date_input2.getTime() == date_input2.getTime()){
            if (date_input <= date_input2){
                document.getElementById('confirmSatelliteDate').disabled = false;
                document.getElementById('userDateStart').value = dateformat(inputDate);
                document.getElementById('userDateEnd').value = dateformat(inputDate2);
            }else{
                document.getElementById('searchMessage').innerHTML = 'The start date should be before the end date.';
            }
        }
    }else{
        document.getElementById('confirmDate').disabled = true;
        document.getElementById('searchMessage').innerHTML = 'Invalid Date';
    }
};

function dateformat(date) {
  date = new Date(date);

  var day = ('0' + date.getDate()).slice(-2);
  var month = ('0' + (date.getMonth() + 1)).slice(-2);
  var year = date.getFullYear();

  return year + '/' + month + '/' + day;
}

// Function to check the satellite input for a satellite search
function satelliteConfirm(){
    var satelliteInput = document.getElementById('userSatellite').value.trim();
    if (satelliteInput == ""){
        document.getElementById('confirmSatellite').disabled = true;
    }else{
        document.getElementById('confirmSatellite').disabled = false;
    }
}

// Function to show the user wants to search a specific date
function userSearchData(searchValue){
    // Check if the search block is visible, if not make it visible or vice-versa to hide it.
    var visible = document.getElementById('UserSearch').style.display;
    if (visible == "none"){
        document.getElementById('UserSearch').style.display = "block";
    }

   // depending on the searchValue different blocks within the search bar should pop up
   // searchValue == 0 is just a date
   if (searchValue == 0){
        document.getElementById('UserDateSearch').style.display="block";
        document.getElementById('UserSatelliteSearch').style.display="none";
        document.getElementById('UserSatelliteDateSearch').style.display="none";
   // searchValue == 1 is a selection of satellites
   }else if (searchValue == 1){
        document.getElementById('UserDateSearch').style.display="none";
        document.getElementById('UserSatelliteSearch').style.display="block";
        document.getElementById('UserSatelliteDateSearch').style.display="none";
   // searchValue == 2 is a selection of satellites and a date range
   }else if (searchValue == 2){
        document.getElementById('UserDateSearch').style.display="none";
        document.getElementById('UserSatelliteSearch').style.display="none";
        document.getElementById('UserSatelliteDateSearch').style.display="block";
   // searchValue == 3 or other is a cancel of the search
   }else{
        document.getElementById('UserSearch').style.display = "none";
        document.getElementById('searchMessage').innerHTML = '';
        document.getElementById('userDate').value = 'yyyy/mm/dd';
        document.getElementById('userDateStart').value = 'yyyy/mm/dd';
        document.getElementById('userDateEnd').value = 'yyyy/mm/dd';
   }


};

// function to determine the x,y,z of subsolar point at t after the initial coordinates
function sunPosition(t, solarLong, solarLat){
    omega_earth = 7.2921159e-5;// rad/s

    	// this does not take the Earth's angle to the Sun into account. Thus this is not accurate over long periods of time.
    lat = solarLat;
    lon = solarLong - omega_earth*t;

    lightX=Math.cos(lon)*Math.cos(lat);
	lightY=Math.sin(lon)*Math.cos(lat);
    lightZ=Math.sin(lat);

    return [lightX, lightY, lightZ];
};

function addSatellites(){
    try{
        scene.remove(scene.getObjectByName('satellites'));
    }catch(ex){}

    try{
        while (display_orbit.children.length >0) {
            display_orbit.remove(display_orbit.children[0]);
        }
    }catch(ex){}

    [satellites, ids, satellite_orbits] = readData(GMT_time.getTime());

    var orbits_specific_ids = [];
    for (var i in specific_ids){
        orbits_specific_ids.push(specific_ids[i][0]);
    }
    console.log(orbits_specific_ids);

    specific_ids = [];
    for (var i in ids){
        if (orbits_specific_ids.indexOf(ids[i][0]) != -1){
            console.log(i, ids[i], ids)
            specific_ids.push(ids[i]);
            var orbit_available = search_satellites.getObjectByName("search_"+ids[i][0]);
            if (typeof(orbit_available) != "undefined"){
                search_satellites.remove(search_satellites.getObjectByName("search_"+ids[i][0]));
                var extra_orbit = satellite_orbits[i].clone();
                extra_orbit.material = new THREE.LineBasicMaterial({color: 0xFFFF00});
                extra_orbit.name = "search_"+ ids[i][0];
                search_satellites.add(extra_orbit);
            }
        }
    }

    update_used_ids = true;
    console.log(ids.length)
//    specific_ids [id for id in ids if id[0] ]

    // test to check if there are satellites
    if (satellites != 0){
        satellites.name = 'satellites';
        scene.add(satellites);
//        searchSatellite();
    }
};

function selectGroup(group){
    index = groups.indexOf(group);
    if (groups.indexOf(group) == -1){
        groups.push(group);
    }else{
        groups.splice(index, 1);
    }
};

function searchSatellite(){
    for (var search in search_list){
        search_num = search_list[search]
        document.getElementById('button_' + search_num[1]).click();
        document.getElementById('position_' + search_num[1]).click();
        while (display_orbit.children.length >0) {
            display_orbit.remove(display_orbit.children[0]);
        }

    }
};

function cameraView(orbit_id){
    var test_string = 'orbit_'+orbit_id;
    var position_in_id = $.inArray(test_string, arrayColumn(ids, 0))

    var object = ids[position_in_id];
    var object_userData = object[1];
    var time_0 = object_userData.start_time;
    var time_delta = (solar_delta)/60;
    var Lx = object_userData.Lx;
    var Ly = object_userData.Ly;
    var Lz = object_userData.Lz;


    var pos_x = 0, pos_y = 0, pos_z = 0, status_x = 0;
     try {
        [pos_x,status_x] = Lx.position(time_delta);
        [pos_y,status_x] = Ly.position(time_delta);
        [pos_z,status_x] = Lz.position(time_delta);
    }catch(ex){
        return
    }

    camera.position.x = 1.5*pos_x;
    camera.position.y = 1.5*pos_y;
    camera.position.z = 1.5*pos_z;

    document.getElementById('satellite_hover').innerHTML = satcat[parseInt(object_userData.satnum)-1]['SATNAME']

    particle_size = 0.1;
    var button = document.getElementById('show_'+orbit_id);

    if (button.color_current == 'white'){
        button.color_current = 'green';
        satellites.geometry.colors[position_in_id] = new THREE.Color(0xF1EA0F);
        satellites.geometry.colorsNeedUpdate = true;
    }else{
        button.color_current = 'white';

        // determine color of object
        var object_type = satcat[orbit_id-1]['OBJECT_TYPE'];
        if (object_type == "PAYLOAD"){
            var color = new THREE.Color(0x00FF00);
        }else if (object_type == 'DEBRIS'){
            var color = new THREE.Color(0xFF0000);
        }else if (object_type == 'ROCKET BODY'){
            var color = new THREE.Color(0xF000F0);
        }else{
            var color = new THREE.Color(0x0000FF);
        }
        satellites.geometry.colors[position_in_id] = color;
        satellites.geometry.colorsNeedUpdate = true;
    }

    satellites.material.size = particle_size;
};

function orbitShow(orbit_id){
    var test_string = 'orbit_'+orbit_id;
    var position_in_id = $.inArray(test_string, arrayColumn(ids, 0))

    var button = document.getElementById('button_'+orbit_id);

    // change orbit button color
    if (button.color_current == 'white'){
        button.style = "-webkit-filter: grayscale(100%) brightness(40%) sepia(100%) hue-rotate(50deg) saturate(1000%)" +
    "contrast(0.8);      filter: grayscale(100%) brightness(40%) sepia(100%) hue-rotate(50deg) saturate(1000%) contrast(0.8);";
        button.color_current = 'green';
        try{
            var extra_orbit = satellite_orbits[position_in_id].clone();
            extra_orbit.material = new THREE.LineBasicMaterial({color: super_colors[super_count%super_colors.length]});

            extra_orbit.name = "search_"+ test_string;
            search_satellites.add(extra_orbit);
        }catch(ex){}
    }else{
        button.style = "";
        button.color_current = 'white';
        search_satellites.remove(search_satellites.getObjectByName("search_"+test_string));
    }
};

function positionShow(orbit_id){
    var test_string = 'orbit_'+orbit_id;
    var position_in_id = $.inArray(test_string, arrayColumn(ids, 0))

    var button = document.getElementById('position_'+orbit_id);

    // change orbit button color
    if (button.color_current == 'white'){
        button.style = "-webkit-filter: grayscale(100%) brightness(40%) sepia(100%) hue-rotate(50deg) saturate(1000%)" +
    "contrast(0.8);      filter: grayscale(100%) brightness(40%) sepia(100%) hue-rotate(50deg) saturate(1000%) contrast(0.8);";
        button.color_current = 'green';

        var object = ids[position_in_id];
        try{
            var object_userData = object[1];

            specific_ids.push(ids[position_in_id]);
            update_used_ids = true;
            satellites.geometry.colorsNeedUpdate = true;
        }catch(ex){}

    }else{
        button.style = "";
        button.color_current = 'white';

        // Find and remove item from an specific_ids
        var id_num = specific_ids.indexOf(ids[position_in_id]);
        if(id_num != -1) {
            specific_ids.splice(id_num, 1);
        }
        update_used_ids = true;
    }
};
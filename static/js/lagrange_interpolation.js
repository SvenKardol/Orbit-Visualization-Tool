var Lagrange = function(time, pos){
    if (time.length < 2 || pos.length < 2 || time.length != pos.length){
        return 'error';
    }

    this.time = time;
	this.pos = pos;
	this.weights = [];
	this.updateWeights();
}

Lagrange.prototype.updateWeights = function(){
    var weight;
    var k = this.time.length

	for (var j = 0; j < k; j++) {
		weight = 1;
		for (var i = 0; i < k; ++i) {
			if (i != j) {
				weight *= this.time[j] - this.time[i];
			}
		}
		this.weights[j] = 1/weight;
	}
}

Lagrange.prototype.position = function(x) {
	var factor = 0;
	var top_sum = 0;
	var bottom_sum = 0;

	for (var j = 0; j < this.time.length; ++j) {
		if (x != this.time[j]) {
			factor = this.weights[j] / (x - this.time[j]);
			top_sum += factor * this.pos[j];
			bottom_sum += factor;
		} else {
			return [this.pos[j], 0];
		}
	}

	var update = 0;
	if (x < this.time[1] || x > this.time[this.time.length-2]){
	    update = 1;
	}

	return [top_sum / bottom_sum, update];
}


//----------------------------------------------------------------------------------------------------------------------
// Made by Sven Kardol
// 6/11/17
// Satellite objects from TLE information for time
//----------------------------------------------------------------------------------------------------------------------


// number of segments for orbits and pre-calculation.
var segments = 100;

// Simple Earth low resolution
function createSatelliteOrbit(tle1, tle2, GMT){

    // determine number of points
    if (buffer_orbit == 1){
        points_used = buffer_time*segments;
    }else{
        points_used = buffer_time;

    }

    if (points_used < 11){
        points_used = 11;
    }

    // satellite record
    var satrec = satellite.twoline2satrec(tle1, tle2, GMT_time);

    // check for errors in the tle. This results in possible problems in the code
    if (satrec.error == 0){
        // create satellite object
        sat = new THREE.Object3D();
        sat.userData = satrec;
        sat.name = 'orbit_'+parseInt(satrec.satnum,10);
        sat.id_name = parseInt(satrec.satnum,10);


        // make empty list for orbit points
        var points = [], satTime = [], satPos = [];
        [points, satTime, satPos] = satelliteBuffer(satrec, GMT);

        // test for a wrong response from the satelliteBuffer.
        if (points == 0){
            return 0;
        }

        // plot the orbit: create spline using CatmullRomCurve3
        spline = new THREE.CatmullRomCurve3(points);

        // create material (aka color in this case)
        var material = new THREE.LineBasicMaterial({
            color: 0x0000FF,
        });

        // set geometry
        var geometry = new THREE.Geometry();

        // spline points
        var splinePoints = spline.getPoints(points_used+1);

        // push the vertices with the points
        for(var i = 0; i < splinePoints.length; i++){
            geometry.vertices.push(splinePoints[i]);
        }

        // create line and add to the object.
        var line = new THREE.Line(geometry, material);
        line.name = 'line';
        sat.orbit = line;

        // determine the Lagrange interpolation points that need to be used
        var time_0 = GMT;
        var length_time = satTime.length/2;

        var posx = [], posy = [], posz = [], time = [];
        for (var i = Math.floor(length_time-5); i <= Math.ceil(length_time+5); i++) {
            var position = satPos[i];
            try{
                posx.push(position.x/1000);
                posy.push(position.y/1000);
                posz.push(position.z/1000);
                time.push((satTime[i]-time_0)/1000/60)
            }catch(ex){
                return 0;
            }
        }

        Lx = new Lagrange(time, posx);
        Ly = new Lagrange(time, posy);
        Lz = new Lagrange(time, posz);

        sat.userData.position = satPos;
        sat.userData.start_time = time_0;
        sat.userData.Lx = Lx;
        sat.userData.Ly = Ly;
        sat.userData.Lz = Lz;
        sat.userData.time = satTime;
        return sat;
    }else{
        return 0;
    }
};

function satelliteUpdate(sat, GMT){
    // retrieve information about the satellite
    var satTime = sat.time;
    var satPos = sat.position;
    var time_0 = sat.start_time;
    var arr = sat.time;

    // get the nearest index to the time for new center of the lagrage calculations
    try{
        index = returnNearestIndex(arr, GMT);
    }catch(ex){
        return 0;
    }

    if (index < 5 || index > arr.length - 6){
       return updateOrbitCalculation(sat, GMT);
    }else{
        orbit = 0;
    }

    var posx = [], posy = [], posz = [], time = [];
    for (var i = Math.floor(index-5); i <= Math.ceil(index+5); i++) {
        var position = satPos[i]
        posx.push(position.x/1000);
        posy.push(position.y/1000);
        posz.push(position.z/1000);
        time.push((satTime[i]-time_0)/1000/60)
    }

    Lx = new Lagrange(time, posx);
    Ly = new Lagrange(time, posy);
    Lz = new Lagrange(time, posz);

    sat.Lx = Lx;
    sat.Ly = Ly;
    sat.Lz = Lz;

    return [sat, orbit];
};

function updateOrbitCalculation(sat, GMT){
    // determine the amount of time simulated
    satrec = sat;

    if (buffer_orbit == 1){
        var period = (2 * Math.PI) / satrec.no  //convert rads/min to min
        var delta_t = buffer_time*period/ points_used; //50 segments per orbit
    }else{
        var delta_t = 1; // 1 minute
    }

    // make empty list for orbit points
    var points = [], satTime = [], satPos = [];
    [points, satTime, satPos] = satelliteBuffer(satrec, GMT);
    if (points == 0){
        return 0;
    }

    // plot the orbit: create spline using CatmullRomCurve3
    spline = new THREE.CatmullRomCurve3(points);

    // create material (aka color in this case)
    var material = new THREE.LineBasicMaterial({
        color: 0xff00f0,
    });

    // set geometry
    var geometry = new THREE.Geometry();

    // spline points
    var splinePoints = spline.getPoints(points_used+1);

    // push the vertices with the points
    for(var i = 0; i < splinePoints.length; i++){
        geometry.vertices.push(splinePoints[i]);
    }

    // create line and add to the object.
    var line = new THREE.Line(geometry, material);
    line.name = 'line';

    // determine the Lagrange interpolation points that need to be used
    var time_0 = GMT;
    var length_time = satTime.length/2;

    var posx = [], posy = [], posz = [], time = [];
    for (var i = Math.floor(length_time-5); i <= Math.ceil(length_time+5); i++) {
        var position = satPos[i]
        posx.push(position.x/1000);
        posy.push(position.y/1000);
        posz.push(position.z/1000);
        time.push((satTime[i]-time_0)/1000/60)
    }

    Lx = new Lagrange(time, posx);
    Ly = new Lagrange(time, posy);
    Lz = new Lagrange(time, posz);

    sat.position = satPos;
    sat.Lx = Lx;
    sat.Ly = Ly;
    sat.Lz = Lz;
    sat.time = satTime;

    return [sat,line];

}

function satelliteBuffer(satrec, GMT){
    // determine the amount of time simulated
    if (buffer_orbit == 1){
        var period = (2 * Math.PI) / satrec.no  //convert rads/min to min
        var delta_t = buffer_time*period/ points_used; //50 segments per orbit
    }else{
        var delta_t = 1; // 1 minute
    }

    // make empty list for orbit points
    var points = new Array((points_used + 1));

    // loop over the number of steps to get the track a sattelite will follow for the desired orbits
    var satPos = [];
    var satVel = [];
    var satTime = []
    for(var i=0; i < points_used + 1; i++) {
        // start half the points earlier (time before and ahead of time
        var t = new Date(GMT + (i-points_used/2)*delta_t*60*1000);
        var po = satellite.propagate(satrec, t);
        try{
            if(reference_frame == 'ECEF'){
                var gmst = satellite.gstimeFromDate(t)
                var p = satellite.eciToEcf(po['position'], gmst);
            }else{
                var p = po['position'];
            }
        }catch(ex){
            return [0, 0, 0];
        }

        satPos.push(p)
        satTime.push(t.getTime());
        try {
            points[i] = new THREE.Vector3(p.x/1000, p.y/1000, p.z/1000);
        } catch (ex) {
            points[i] = new THREE.Vector3(0, 0, 0);
        }
    }

    return [points, satTime, satPos];
}

// function that returns the nearestIndex using a smart algorithm skipping about 75% of the numbers
function returnNearestIndex(array, value){
    var length = array.length, size = Math.floor(length/2), index = size;

    // search array by halving the size to zero it down to a single index
    while (size > 1){
        size = Math.floor(size/2);
        if (value < array[index]){
            index -= size;
        }else{
            index += size;
        }
    }

    // compensate for floor function. If the item is found past half the array, time is going forward, meaning the next
    // index would represent a better value for the next langrange interpolation.
    if (index > length/2){
        index += 1;
    }

    return index;
}
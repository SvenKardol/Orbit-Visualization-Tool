//show time stamp
var time_stamp = document.getElementById('time');
time_stamp.style.display = "block";

// get the menu place
var user_div = document.getElementById('user_inputs');

// time factor div
var time_div = document.createElement("DIV");
var time_p = document.createElement("P");
var time_text = document.createTextNode("Jump to time: ");
var time_input = document.createElement('INPUT');

time_input.className = 'timepicker2';
time_input.id = 'time_input'
time_input.type = 'text';
time_input.style.width = '50px';
time_p.appendChild(time_text);
time_div.appendChild(time_p);
time_div.appendChild(time_input);

var time_confirm = document.createElement("BUTTON");
var image_confirm = document.createElement("IMG");
image_confirm.className='button_image';
image_confirm.src="../static/img/confirm.png";
time_confirm.appendChild(image_confirm);
time_confirm.onclick = function(){setTime(document.getElementById('time_input'));};
time_div.appendChild(time_confirm);
user_div.appendChild(time_div)

$('.timepicker2').timepicker();


// time factor div
var time_factor_div = document.createElement("DIV");
var time_factor_p = document.createElement("P");
var time_factor_text = document.createTextNode("Time Acceleration:     ");
time_factor_p.appendChild(time_factor_text)

var time_factor_user_div = document.createElement("DIV");
time_factor_user_div.className = "buttons";
var time_factor_down = document.createElement("BUTTON");
var image_minus = document.createElement("IMG");
image_minus.className='button_image';
image_minus.src="../static/img/minus.png";
time_factor_down.appendChild(image_minus);
time_factor_down.onclick = function(){timeFactor(-1);};

var time_factor_value_p = document.createElement("P");
var time_factor_value = document.createTextNode("1");
time_factor_value_p.id = 'time_factor_value';
time_factor_value_p.appendChild(time_factor_value)

var time_factor_up = document.createElement("BUTTON");
var image_plus = document.createElement("IMG");
image_plus.className='button_image'
image_plus.src="../static/img/plus.png";
time_factor_up.appendChild(image_plus);
time_factor_up.onclick = function(){timeFactor(1);};

time_factor_user_div.appendChild(time_factor_down);
time_factor_user_div.appendChild(time_factor_value_p);
time_factor_user_div.appendChild(time_factor_up);

time_factor_div.appendChild(time_factor_p);
time_factor_div.appendChild(time_factor_user_div);

user_div.appendChild(time_factor_div);

// change Earth
var change_earth_div = document.createElement("DIV");
var change_earth_p = document.createElement("P");
var change_earth_text = document.createTextNode("Change Earth Look: ");
change_earth_p.appendChild(change_earth_text)

var change_earth_user_div = document.createElement("DIV");
change_earth_user_div.className = "buttons";
var change_earth_button = document.createElement("BUTTON");
var image_earth = document.createElement("IMG");
image_earth.className='button_image';
image_earth.src="../static/img/earth.png";
change_earth_button.appendChild(image_earth);
change_earth_button.onclick = function(){changeEarth()};

change_earth_div.appendChild(change_earth_p);
change_earth_div.appendChild(change_earth_button);

user_div.appendChild(change_earth_div);

// change particle size
var particle_size_div = document.createElement("DIV");
var particle_size_p = document.createElement("P");
var particle_size_text = document.createTextNode("Particle Size:            ");
particle_size_p.appendChild(particle_size_text)

var particle_size_user_div = document.createElement("DIV");
particle_size_user_div.className = "buttons";
var particle_size_down = document.createElement("BUTTON");
var image_minus2 = document.createElement("IMG");
image_minus2.className='button_image';
image_minus2.src="../static/img/minus.png";
particle_size_down.appendChild(image_minus2);
particle_size_down.onclick = function(){particleSize(-1);};

var particle_size_value_p = document.createElement("P");
var particle_size_value = document.createTextNode(particle_size);
particle_size_value_p.id = 'particle_size_value';
particle_size_value_p.appendChild(particle_size_value)

var particle_size_up = document.createElement("BUTTON");
var image_plus2 = document.createElement("IMG");
image_plus2.className='button_image'
image_plus2.src="../static/img/plus.png";
time_factor_up.appendChild(image_plus2);
particle_size_up.appendChild(image_plus2);
particle_size_up.onclick = function(){particleSize(1);};

particle_size_user_div.appendChild(particle_size_down);
particle_size_user_div.appendChild(particle_size_value_p);
particle_size_user_div.appendChild(particle_size_up);

particle_size_div.appendChild(particle_size_p);
particle_size_div.appendChild(particle_size_user_div);

user_div.appendChild(particle_size_div);

// change reference frame
var reference_frame_div = document.createElement("DIV");
var reference_frame_p = document.createElement("P");
var reference_frame_text = document.createTextNode("Current reference frame: " + reference_frame);
reference_frame_p.id = 'reference_frame_value';
reference_frame_p.appendChild(reference_frame_text);

var reference_frame_change = document.createElement("BUTTON");
var image_switch = document.createElement("IMG");
image_switch.className='button_image';
image_switch.src="../static/img/switch.png";
reference_frame_change.appendChild(image_switch);
reference_frame_change.onclick = function(){referenceFrame();};

reference_frame_div.appendChild(reference_frame_p);
reference_frame_div.appendChild(reference_frame_change);

user_div.appendChild(reference_frame_div);


// change visible_information
var visible_information_div = document.createElement("DIV");
var visible_information_p = document.createElement("P");
var visible_information_text = document.createTextNode("Toggle information ");
visible_information_p.id = 'rvisible_information_value';
visible_information_p.appendChild(visible_information_text);

var visible_information_change = document.createElement("BUTTON");
var image_switch2 = document.createElement("IMG");
image_switch2.className='button_image';
image_switch2.src="../static/img/switch.png";
visible_information_change.appendChild(image_switch2);
visible_information_change.onclick = function(){toggleInformation();};

visible_information_div.appendChild(visible_information_p);
visible_information_div.appendChild(visible_information_change);

user_div.appendChild(visible_information_div);

// time slider
var slider_div = document.createElement("DIV");
slider_div.className = 'slidecontainer';
slider_div.id = 'slider_div'
slider_div.value = 1200;
var slider_input = document.createElement("INPUT")
slider_input.type = "range";
slider_input.min = "1";
slider_input.max = "2400";
slider_input.value = "1200";
slider_input.innerHTML = 'test'
slider_input.className = "slider";
slider_input.id = "myRange";

slider_input.oninput = function(){
    time_offset = this.value - document.getElementById('slider_div').value
    document.getElementById('slider_div').value = this.value;
}
slider_div.appendChild(slider_input);
document.getElementById('user_menu').appendChild(slider_div);

//
//// Time down by hour
//var btnTimeDown = document.createElement("BUTTON");
//var t = document.createTextNode("Time - 1 hr");
//btnTimeDown.appendChild(t);
//document.getElementById('user_menu').appendChild(btnTimeDown);
//
//// Time up by hour
//var btnTimeUp = document.createElement("BUTTON");
//var t = document.createTextNode("Time + 1 hr");
//btnTimeUp.appendChild(t);
//document.getElementById('user_menu').appendChild(btnTimeUp);

////time scaling factor
//var bufferTimeText = document.createElement("input");
//bufferTimeText.value = '1';
//bufferTimeText.size = 1;
//document.getElementById('user_menu').appendChild(bufferTimeText);
//
////time scaling factor
//var bufferOrbitText = document.createElement("input");
//bufferOrbitText.value = '1';
//bufferOrbitText.size = 1;
//document.getElementById('user_menu').appendChild(bufferOrbitText);
//
//var btnBuffer = document.createElement("BUTTON");bufferTimeText
//var t = document.createTextNode("Change Buffer");
//btnBuffer.appendChild(t);
//document.getElementById('user_menu').appendChild(btnBuffer);

function timeFactor(factor){
    for (var i in time_factors){
         if (time_factors[i] == time_factor && (parseInt(i)+factor)>-1 && (parseInt(i)+factor)<time_factors.length){
            var b = parseInt(i);
            b = b + factor;
            time_factor = time_factors[b];
            document.getElementById('time_factor_value').innerHTML = time_factor;
            return;
         }
    }
};


function changeEarth(){
    scene.remove(earth)
    if (earth.type == 'simple'){
        earth = earth_complex;
    }else{
        earth = earth_simple;
    }

    scene.add(earth)
};

function particleSize(factor){
    particle_size += (factor);
    if (particle_size < 1){
        particle_size = 1
    }
    if (particle_size > 10){
        particle_size = 10;
    }
    satellites.material.size = Math.round(particle_size*1)/10;
    document.getElementById('particle_size_value').innerHTML =  Math.round(particle_size);
};

//btnTimeDown.onclick = function(){
//    time_offset = -3600;
//}
//btnTimeUp.onclick=function(){
//    time_offset = 3600;
//}

function referenceFrame(){
    if (reference_frame == "ECI"){
        reference_frame = "ECEF";
        document.getElementById('reference_frame_value').innerHTML = "Current reference frame: ECEF";
        while (display_orbit.children.length > 0) {
            display_orbit.remove(display_orbit.children[0]);
        }
        addSatellites();
    }else{
        reference_frame = "ECI";
        document.getElementById('reference_frame_value').innerHTML = "Current reference frame: ECI";
        while (display_orbit.children.length > 0) {
            display_orbit.remove(display_orbit.children[0]);
        }
        addSatellites();
    }
}

function toggleInformation(){
    temp_div = document.getElementById('satellite_hover')

    if (temp_div.style.display == 'block'){
        document.getElementById('satellite_hover').style.display = 'none';
        document.getElementById('satellite_info').style.display = 'none';
        information_visible = false;
        while (display_orbit.children.length > 0) {
            display_orbit.remove(display_orbit.children[0]);
        }
    }else{
        document.getElementById('satellite_hover').style.display = 'block';
        document.getElementById('satellite_info').style.display = 'block';
        information_visible = true;
    }
}

function setTime(time_input){
    console.log(time_input.value);
    if(time_input.value != ""){
        var hours = time_input.value.split(':').slice(0);
        var minutes = time_input.value.split(':').slice(1);

        // get current simulation time
        var simulation_time = new Date(simulation)
        var sim_hours = simulation_time.getUTCHours();
        var sim_minutes = simulation_time.getMinutes();

        // calculate the time_offset
        time_offset = (parseInt(hours)*3600+parseInt(minutes)*60)-(sim_hours*3600+sim_minutes*60)
        console.log(time_offset)
    }
}

// set delay on search of 500 ms.
// Get the input box
var textInput = document.getElementById('satellite_search');

// Init a timeout variable to be used below
var timeout = null;

// Listen for keystroke events
textInput.onkeyup = function (e) {
    // Clear the timeout if it has already been set.
    // This will prevent the previous task from executing
    // if it has been less than 500 ms
    clearTimeout(timeout);

    // Make a new timeout set to go off in 500ms
    timeout = setTimeout(function () {
        searchSatelliteResults(textInput.value);
    }, 500);
};
//
//btnBuffer.onclick = function(){
//    try{
//        buffer_time = parseInt(bufferTimeText.value);
//        buffer_orbit = parseInt(bufferOrbitText.value);
//        addSatellites();
//    }catch(ex){}
//}
// satellite record
tle1_04 = '1 26405U 00039B   04153.70084197  .00008066  00000-0  85439-4 0  9991';
tle2_04 = '2 26405  87.2427 328.3509 0003637 125.8429 234.3151 15.63896626219833';

tle1_08 = '1 26405U 00039B   08001.96095000 +.00006167 +00000-0 +35718-4 0  9995';
tle2_08 = '2 26405 087.2326 168.7078 0002955 078.6774 281.4827 15.78250960425505';

var satrec = satellite.twoline2satrec(tle1_08, tle2_08);

console.log(satrec);

data = [];
omega = -7.29211e-5;
console.log(omega)

for (i = 0; i < 8641; i++) {
    t = i
    var utcDate1 = new Date(Date.UTC(2008, 0, parseInt(t/8640)+1, parseInt(t/360)%24, parseInt(t/6)%60, (t*10)%60));
    //  Propagate satellite using time since epoch (in minutes).
    var positionAndVelocity = satellite.propagate(satrec, utcDate1);

    var gmst = satellite.gstimeFromDate(utcDate1);

    var positionEci = positionAndVelocity.position,
        velocityEci = positionAndVelocity.velocity;

    // You can get ECF, Geodetic, Look Angles, and Doppler Factor.
    var positionEcf   = satellite.eciToEcf(positionEci, gmst),
        velocityEcf   = satellite.eciToEcf(velocityEci, gmst);

    rx = positionEcf.x*1000;
    ry = positionEcf.y*1000;
    rz = positionEcf.z*1000;
    vx = velocityEcf.x*1000 - omega*ry;
    vy = velocityEcf.y*1000 + omega*rx;
    vz = velocityEcf.z*1000;


    data.push([rx, ry, rz, vx, vy, vz]);
}
document.getElementById('satellite_hover').innerHTML = data;

var csvContent = '';
data.forEach(function(infoArray, index) {
  dataString = infoArray.join(';');
  csvContent += index < data.length ? dataString + '\n' : dataString;
});

// The download function takes a CSV string, the filename and mimeType as parameters
// Scroll/look down at the bottom of this snippet to see how download is called
var download = function(content, fileName, mimeType) {
  var a = document.createElement('a');
  mimeType = mimeType || 'application/octet-stream';

  if (navigator.msSaveBlob) { // IE10
    navigator.msSaveBlob(new Blob([content], {
      type: mimeType
    }), fileName);
  } else if (URL && 'download' in a) { //html5 A[download]
    a.href = URL.createObjectURL(new Blob([content], {
      type: mimeType
    }));
    a.setAttribute('download', fileName);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  } else {
    location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
  }
}

download(csvContent, 'dowload.csv', 'text/csv;encoding:utf-8');
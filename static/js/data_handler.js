//----------------------------------------------------------------------------------------------------------------------
// Made by Sven Kardol
// 6/11/17
// Handle the data that is send from the python server to be involved in the javascript. This includes the TLE
// information from satellites, the satellite catalog for more information.
//----------------------------------------------------------------------------------------------------------------------

// Function to read data from the data text box.
function readData(GMT_time){
    // read data from text box\
    var raw_data = document.getElementById('data').innerHTML;
    var satcat_temp = document.getElementById('satcat').innerHTML;

    // replace all ' by " to be able to parse as JSON
    raw_data = raw_data.replace(/\'/g , "\"");

    try{
        satcat = JSON.parse(satcat_temp);
    }catch(ex){
        console.log('satcat parsing error.');
    }


    if (raw_data=='None'){
        console.log('No data is available.')
        document.getElementById('data').innerHTML='No data available'
        return [0,0];
    }else{
        try{
            var particle_system_geometry = new THREE.Geometry();

//            // Code for using sprites, but not working the way it should.
//            // depthTest causes particles to be visible behind objects
//            var particle_system_material = new THREE.PointsMaterial({
//                vertexColors : true,
//                map : THREE.ImageUtils.loadTexture('../static/img/dot.png'),
////                color: 0x2ECC71,
//                transparent: true,
//                size: particle_size,
//                blending: THREE.AdditiveBlending,
//                sizeAttenuation: true,
//                depthTest: false,
//            });

             var particle_system_material = new THREE.PointsMaterial({
                vertexColors : true,
                transparent: true,
                size: particle_size/10,
                sizeAttenuation: true,
            });

//            var color_payload = new THREE.Color(0x00FF00); //0x2ECC71
//            var color_debris = new THREE.Color(0xFF0000);
//            var color_rb = new THREE.Color(0xF000F0);
//            var color_other = new THREE.Color(0x0000FF);

            var satellite_orbits = [];
            var ids = [];
            // parse JSON
            var data = JSON.parse(raw_data);


            count = 0
            // Loop through json
            for (var  key in data) {
                var obj = data[key];
                count+=1;

                // Sample TLE
                var tleLine1 = obj['tle1'],  tleLine2 = obj['tle2'];

                // create satellite object
                temp_sat = createSatelliteOrbit(tleLine1,tleLine2, GMT_time);
                if (temp_sat != 0){
                    temp_sat_position = temp_sat.userData.position[0];

                    try{
                        var object_type = satcat[temp_sat.id_name-1]['OBJECT_TYPE'];
                    }catch(ex){
                        var object_type = "TBA";
                    }

                    vector = new THREE.Vector3(temp_sat_position['x']/1000, temp_sat_position['y']/1000,
                                                temp_sat_position['z']/1000);

                    if (groups.indexOf(object_type) != -1){
                        try{
                            particle_system_geometry.colors.push(group_colors[object_type]);
                        }catch(ex){
                            particle_system_geometry.colors.push(group_colors['TBA']);
                        }
//                        if (object_type =='PAYLOAD'){
//                            particle_system_geometry.colors.push(color_payload);
//                        }else if(object_type =='DEBRIS'){
//                            particle_system_geometry.colors.push(color_debris);
//                        }else if(object_type == 'ROCKET BODY'){
//                            particle_system_geometry.colors.push(color_rb)
//                        }else{
//                            particle_system_geometry.colors.push(color_other);
//                        }


                        particle_system_geometry.vertices.push(vector);
                        satellite_orbits.push($.extend(true, {},temp_sat.orbit));
                        ids.push([temp_sat.name, temp_sat.userData]);
                    }
                };
            };

            var particleSystem = new THREE.Points(
                particle_system_geometry,
                particle_system_material
            );
            particleSystem.sizeAttenuation = true;
            console.log(count);
            return [particleSystem, ids, satellite_orbits];
        }catch(ex){
            console.log('something went wrong loading the data',ex);
            return [0,0];
        }
    }

};
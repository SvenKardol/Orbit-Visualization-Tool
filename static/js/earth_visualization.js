//----------------------------------------------------------------------------------------------------------------------
// Made by Sven Kardol
// 1/11/17
// Create Earth. Simple version is a small sphere with low resolution image. Regular one has higher resolution with
// night lights, clouds, water reflections, rivers, and bump map.
//----------------------------------------------------------------------------------------------------------------------


// Earth paramaters
var radius   = 6.371, // in thousand km
    segments = 64;

// Simple Earth low resolution
function createSimpleEarth(){

    // create a Object3D
    var earth_total = new THREE.Object3D();

    // create the Mesh for Earth
    var earth = new THREE.Mesh(
                    new THREE.SphereGeometry(radius, segments, segments),
                    new THREE.MeshBasicMaterial({
                        map:    THREE.ImageUtils.loadTexture('../static/img/earth_simple.jpg')
                    })
        );

	// add the earth to the total object
	earth_total.add(earth);

	// position the object and rotate so that the axis will line up with cartesian coordinates (right handed, z-axis up)
    earth.position.set(0, 0, 0);
    earth.rotation.x = Math.PI*0.5;
    earth_total.name = 'earth_simple';
    earth_total.type = 'simple';

    // return the object
	return earth_total;
};

// High resolution Earth for more cinematic screenshots
function createEarth(){
    // create a Object3D
    var earth_total = new THREE.Object3D();

    // create the Mesh for Earth
    var earth = new THREE.Mesh(
                    new THREE.SphereGeometry(radius, segments, segments),
                    new THREE.MeshPhongMaterial({
                        map:         THREE.ImageUtils.loadTexture('../static/img/earth_4k.jpg'),
                        emissiveMap: THREE.ImageUtils.loadTexture('../static/img/earth_night_transp.png'),
                        emissive:    new THREE.Color(0x444444),
                        bumpMap:     THREE.ImageUtils.loadTexture('../static/img/elev_bump_4k.jpg'),
                        bumpScale:   0.05,
                        specularMap: THREE.ImageUtils.loadTexture('../static/img/water_4k.png'),
                        specular:    new THREE.Color(0x444444)
                    })
                )

    // create the Mesh for clouds
    var clouds = new THREE.Mesh(
                    new THREE.SphereGeometry(radius + 0.15, segments, segments),
                    new THREE.MeshPhongMaterial({
                        map:         THREE.ImageUtils.loadTexture('../static/img/fair_clouds_4k.png'),
                        transparent: true
                    })
                );

    // add the earth to the total object
    earth_total.add(earth);
    // position the object and rotate so that the axis will line up with cartesian coordinates (right handed, z-axis up)
    earth.position.set(0, 0, 0);
    earth.rotation.x = Math.PI*0.5;

    // add the clouds to the total object
    earth_total.add(clouds);
    // position the object and rotate so that the axis will line up with cartesian coordinates (right handed, z-axis up)
    clouds.position.set(0, 0, 0);
    clouds.rotation.x = Math.PI*0.5;

    earth_total.name = 'earth_complex';
    earth_total.type = 'complex';

    // return the Object3D
    return earth_total;
};